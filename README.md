# Curso de HTML e CSS - Curso em Vídeo

Este repositório contém o material de estudo utilizado no curso de HTML e CSS da plataforma Curso em Vídeo. O curso é ministrado pelo professor Gustavo Guanabara e tem como objetivo ensinar as linguagens de marcação HTML e CSS de uma forma prática e divertida.

## Estrutura do curso

O curso é dividido em três partes principais:

1. **Módulo 1**: Introdução ao HTML
   - Estrutura básica de um documento HTML
   - Tags HTML
   - Atributos HTML
   - Listas e links
   - Imagens e vídeos

2. **Módulo 2**: Introdução ao CSS
   - Sintaxe CSS
   - Seletores CSS
   - Propriedades CSS
   - Estilização de texto e links
   - Box Model

3. **Módulo 3**: Desenvolvimento de sites
   - Layouts com CSS
   - Responsividade com media queries
   - Formulários HTML
   - Tabelas HTML
   - Projeto final

## Como usar

Este repositório contém os códigos-fonte utilizados nas aulas, bem como os arquivos de exercícios e projetos. Você pode clonar este repositório em sua máquina local e seguir as instruções do curso para aprender HTML e CSS.

## Contribuindo

Se você gostaria de contribuir para este projeto, sinta-se à vontade para enviar um pull request ou entrar em contato comigo diretamente.

## Licença

Este projeto está licenciado sob a licença MIT. Consulte o arquivo LICENSE para obter mais informações.
